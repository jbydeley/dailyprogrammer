package caesar

import "testing"

func TestEncrypt(t *testing.T) {
	actual, expected := Encrypt("Hello, World!"), "Uryyb, Jbeyq!"
	if actual != expected {
		t.Errorf("%v != %v", actual, expected)
	}
}
