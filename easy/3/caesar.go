package caesar

import (
	"bytes"
	"io"
	"strings"
)

type CaesarReader struct {
	r io.Reader
}

func (c CaesarReader) Read(p []byte) (n int, err error) {
	n, err = c.r.Read(p)
	for i := 0; i < len(p); i++ {
		if (p[i] >= 'A' && p[i] < 'N') || (p[i] >= 'a' && p[i] < 'n') {
			p[i] += 13
		} else if (p[i] > 'M' && p[i] <= 'Z') || (p[i] > 'm' && p[i] <= 'z') {
			p[i] -= 13
		}
	}
	return
}
func Encrypt(text string) string {
	var buf bytes.Buffer
	s := strings.NewReader(text)
	c := CaesarReader{s}
	buf.ReadFrom(c)
	return buf.String()
}
