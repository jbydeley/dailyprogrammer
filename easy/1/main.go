package main

import (
	"bufio"
	"fmt"
	"os"
)

var reader = bufio.NewReader(os.Stdin)

func main() {
	name := ask("What is your name? ")
	age := ask("What is your age? ")
	username := ask("What is your username? ")
	fmt.Printf("Your name is %s, you are %s years old, and your username is %s.\n", name, age, username)
}

func ask(q string) string {
	fmt.Printf(q)
	r, _, _ := reader.ReadLine()
	return string(r)
}
