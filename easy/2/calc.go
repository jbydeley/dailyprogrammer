package calc

func CalcForce(m, a int) int {
	return m * a
}

func CalcMass(f, a int) int {
	return f / a
}

func CalcAcceleration(f, m int) int {
	return f / m
}
