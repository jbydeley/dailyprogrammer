package calc

import "testing"

var (
	f, m, a = 6, 2, 3
)

func TestCalcForce(t *testing.T) {
	if CalcForce(m, a) != f {
		t.Errorf("%v * %v should return %v but recieved %v", m, a, f, CalcForce(m, a))
	}
}

func TestCalcMass(t *testing.T) {
	if CalcMass(f, a) != m {
		t.Errorf("%v * %v should return %v but recieved %v", m, a, f, CalcMass(f, a))
	}
}

func TestCalcAcceleration(t *testing.T) {
	if CalcAcceleration(f, m) != a {
		t.Errorf("%v * %v should return %v but recieved %v", m, a, f, CalcAcceleration(f, m))
	}
}
